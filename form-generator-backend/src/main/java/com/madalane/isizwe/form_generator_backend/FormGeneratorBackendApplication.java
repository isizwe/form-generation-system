package com.madalane.isizwe.form_generator_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormGeneratorBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormGeneratorBackendApplication.class, args);
	}

}
